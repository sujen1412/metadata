import typing
import unittest
from collections import OrderedDict

from d3m_metadata import container, hyperparams


class TestHyperparams(unittest.TestCase):
    def test_hyperparameter(self):
        hyperparameter = hyperparams.Hyperparameter[str]('nothing')

        self.assertEqual(hyperparameter.default, 'nothing')
        self.assertEqual(hyperparameter.sample(42), 'nothing')

        self.assertEqual(hyperparameter.to_json(), {
            'default': 'nothing',
            'semantic_types': (),
            'structural_type': str,
            'type': hyperparams.Hyperparameter,
        })

        with self.assertRaisesRegex(TypeError, 'Default value \'.*\' is not an instance of the structural type'):
            hyperparams.Hyperparameter[int]('nothing')

    def test_bounded(self):
        hyperparameter = hyperparams.Bounded[float](0.0, 1.0, 0.2)

        self.assertEqual(hyperparameter.default, 0.2)
        self.assertEqual(hyperparameter.sample(42), 0.2)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 0.2,
            'semantic_types': (),
            'structural_type': float,
            'type': hyperparams.Bounded,
            'lower': 0.0,
            'upper': 1.0,
        })

        with self.assertRaisesRegex(TypeError, 'Default value \'.*\' is not an instance of the structural type'):
            hyperparams.Bounded[str]('lower', 'upper', 0.2)

        with self.assertRaisesRegex(TypeError, 'Lower bound \'.*\' is not an instance of the structural type'):
            hyperparams.Bounded[str](0.0, 'upper', 'default')

        with self.assertRaisesRegex(TypeError, 'Upper bound \'.*\' is not an instance of the structural type'):
            hyperparams.Bounded[str]('lower', 1.0, 'default')

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.Bounded[str]('lower', 'upper', 'default')

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.Bounded[float](0.0, 1.0, 1.2)

        hyperparams.Bounded[typing.Optional[float]](0.0, None, 0.2)
        hyperparams.Bounded[typing.Optional[float]](None, 1.0, 0.2)

        with self.assertRaisesRegex(ValueError, 'Lower and upper bounds cannot both be None'):
            hyperparams.Bounded[typing.Optional[float]](None, None, 0.2)

        with self.assertRaisesRegex(TypeError, 'Default value \'.*\' is not an instance of the structural type'):
            hyperparams.Bounded[float](0.0, 1.0, None)

        with self.assertRaises(TypeError):
            hyperparams.Bounded[typing.Optional[float]](0.0, 1.0, None)

        hyperparams.Bounded[typing.Optional[float]](None, 1.0, None)
        hyperparams.Bounded[typing.Optional[float]](0.0, None, None)

    def test_enumeration(self):
        hyperparameter = hyperparams.Enumeration(['a', 'b', 1, 2, None], None)

        self.assertEqual(hyperparameter.default, None)
        self.assertEqual(hyperparameter.sample(42), 2)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 'None',
            'semantic_types': (),
            'structural_type': typing.Union[str, int, type(None)],
            'type': hyperparams.Enumeration,
            'values': ['a', 'b', 1, 2, 'None'],
        })

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is not among values'):
            hyperparams.Enumeration(['a', 'b', 1, 2], None)

        with self.assertRaisesRegex(TypeError, 'Value \'.*\' is not an instance of the structural type'):
            hyperparams.Enumeration[typing.Union[str, int]](['a', 'b', 1, 2, None], None)

    def test_other(self):
        hyperparameter = hyperparams.UniformInt(1, 10, 2)

        self.assertEqual(hyperparameter.default, 2)
        self.assertEqual(hyperparameter.sample(42), 7)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 2,
            'semantic_types': (),
            'structural_type': int,
            'type': hyperparams.UniformInt,
            'lower': 1,
            'upper': 10,
            'upper_inclusive': False,
        })

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.UniformInt(1, 10, 0)

        hyperparameter = hyperparams.Uniform(1.0, 10.0, 2.0)

        self.assertEqual(hyperparameter.default, 2.0)
        self.assertEqual(hyperparameter.sample(42), 4.370861069626263)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 2.0,
            'semantic_types': (),
            'structural_type': float,
            'type': hyperparams.Uniform,
            'lower': 1.0,
            'upper': 10.0,
            'upper_inclusive': False,
        })

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.Uniform(1.0, 10.0, 0.0)

        hyperparameter = hyperparams.LogUniform(1.0, 10.0, 2.0)

        self.assertEqual(hyperparameter.default, 2.0)
        self.assertEqual(hyperparameter.sample(42), 79.111723081473684)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 2.0,
            'semantic_types': (),
            'structural_type': float,
            'type': hyperparams.LogUniform,
            'lower': 1.0,
            'upper': 10.0,
            'upper_inclusive': False,
        })

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.LogUniform(1.0, 10.0, 0.0)

    def test_union(self):
        hyperparameter = hyperparams.Union(
            OrderedDict(
                none=hyperparams.Hyperparameter(None),
                range=hyperparams.UniformInt(1, 10, 2)
            ),
            'none',
        )

        self.assertEqual(hyperparameter.configuration['none'].name, 'none')

        self.assertEqual(hyperparameter.default, None)
        self.assertEqual(hyperparameter.sample(45), 4)

        self.assertEqual(hyperparameter.to_json(), {
            'default': 'None',
            'semantic_types': (),
            'structural_type': typing.Union[type(None), int],
            'type': hyperparams.Union,
            'configuration': {
                'none': {
                    'default': 'None',
                    'semantic_types': (),
                    'structural_type': type(None),
                    'type': hyperparams.Hyperparameter,
                },
                'range': {
                    'default': 2,
                    'semantic_types': (),
                    'structural_type': int,
                    'type': hyperparams.UniformInt,
                    'lower': 1,
                    'upper': 10,
                    'upper_inclusive': False,
                }
            }
        })

        with self.assertRaisesRegex(TypeError, 'Hyper-parameter name is not a string'):
            hyperparams.Union(OrderedDict({1: hyperparams.Hyperparameter(None)}), 1)

        with self.assertRaisesRegex(TypeError, 'Hyper-parameter description is not an instance of the Hyperparameter class'):
            hyperparams.Union(OrderedDict(none=None), 'none')

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is not in configuration'):
            hyperparams.Union(OrderedDict(range=hyperparams.UniformInt(1, 10, 2)), 'none')

        hyperparams.Union(OrderedDict(range=hyperparams.UniformInt(1, 10, 2), default=hyperparams.Hyperparameter('nothing')), 'default')
        hyperparams.Union[typing.Union[str, int]](OrderedDict(range=hyperparams.UniformInt(1, 10, 2), default=hyperparams.Hyperparameter('nothing')), 'default')

        with self.assertRaisesRegex(TypeError, 'Hyper-parameter \'.*\' is not a subclass of the structural type'):
            hyperparams.Union[str](OrderedDict(range=hyperparams.UniformInt(1, 10, 2), default=hyperparams.Hyperparameter('nothing')), 'default')

    def test_hyperparams(self):
        class TestHyperparams(hyperparams.Hyperparams):
            a = hyperparams.Union(OrderedDict(
                range=hyperparams.UniformInt(1, 10, 2),
                none=hyperparams.Hyperparameter(None),
            ), 'range')
            b = hyperparams.Uniform(1.0, 10.0, 2.0)

        self.assertEqual(TestHyperparams.configuration['a'].name, 'a')

        self.assertEqual(TestHyperparams.defaults(), {'a': 2, 'b': 2.0})
        self.assertEqual(TestHyperparams.defaults(), TestHyperparams({'a': 2, 'b': 2.0}))
        self.assertEqual(TestHyperparams.sample(42), {'a': 4, 'b': 9.556428757689245})
        self.assertEqual(TestHyperparams.sample(42), TestHyperparams({'a': 4, 'b': 9.556428757689245}))
        self.assertEqual(TestHyperparams(TestHyperparams.defaults(), b=3.0), {'a': 2, 'b': 3.0})
        self.assertEqual(TestHyperparams(TestHyperparams.defaults(), **{'b': 4.0}), {'a': 2, 'b': 4.0})

        self.assertEqual(TestHyperparams.to_json(), {
            'a': {
                'default': 2,
                'semantic_types': (),
                'structural_type': typing.Union[int, type(None)],
                'type': hyperparams.Union,
                'configuration': {
                    'none': {
                        'default': 'None',
                        'semantic_types': (),
                        'structural_type': type(None),
                        'type': hyperparams.Hyperparameter,
                    },
                    'range': {
                        'default': 2,
                        'lower': 1,
                        'semantic_types': (),
                        'structural_type': int,
                        'type': hyperparams.UniformInt,
                        'upper': 10,
                        'upper_inclusive': False,
                    },
                },
            },
            'b': {
                'default': 2.0,
                'semantic_types': (),
                'structural_type': float,
                'type': hyperparams.Uniform,
                'lower': 1.0,
                'upper': 10.0,
                'upper_inclusive': False,
            }
        })

        test_hyperparams = TestHyperparams({'a': TestHyperparams.configuration['a'].default, 'b': TestHyperparams.configuration['b'].default})

        self.assertEqual(test_hyperparams['a'], 2)
        self.assertEqual(test_hyperparams['b'], 2.0)

        with self.assertRaisesRegex(ValueError, 'Not all hyper-parameters are specified'):
            TestHyperparams({'a': TestHyperparams.configuration['a'].default})

        with self.assertRaisesRegex(ValueError, 'Additional hyper-parameters are specified'):
            TestHyperparams({'a': TestHyperparams.configuration['a'].default, 'b': TestHyperparams.configuration['b'].default, 'c': 'two'})

        TestHyperparams({'a': 3, 'b': 3.0})
        TestHyperparams({'a': None, 'b': 3.0})

        test_hyperparams = TestHyperparams(a=None, b=3.0)
        self.assertEqual(test_hyperparams['a'], None)
        self.assertEqual(test_hyperparams['b'], 3.0)

        with self.assertRaisesRegex(ValueError, 'Value \'.*\' for hyper-parameter \'.*\' has not validated with any of configured hyper-parameters'):
            TestHyperparams({'a': 0, 'b': 3.0})

        with self.assertRaisesRegex(ValueError, 'Value \'.*\' for hyper-parameter \'.*\' is outside of range'):
            TestHyperparams({'a': 3, 'b': 100.0})

    def test_numpy(self):
        class TestHyperparams(hyperparams.Hyperparams):
            value = hyperparams.Hyperparameter[container.ndarray](
                default=container.ndarray([0]),
            )

        TestHyperparams(value=container.ndarray([1, 2, 3]))


if __name__ == '__main__':
    unittest.main()
