import unittest

import jsonschema

from d3m_metadata import metadata


class TestMetadata(unittest.TestCase):
    def test_basic(self):
        md1 = metadata.Metadata({'value': 'test'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})

        md2 = md1.update((), {'value2': 'test2'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})

        md3 = md2.update(('foo',), {'element': 'one'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})

        md4 = md3.update((metadata.ALL_ELEMENTS,), {'element': 'two'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})

        md5 = md4.update(('foo',), {'element': 'three'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})

        md6 = md5.clear()

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})

        md7 = md6.update((), {'value': 'test'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})
        self.assertEqual(md7.query(()), {'value': 'test'})
        self.assertEqual(md7.query(('foo',)), {})
        self.assertEqual(md7.query(('bar',)), {})

        md8 = md7.clear({'value2': 'test2'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((metadata.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})
        self.assertEqual(md7.query(()), {'value': 'test'})
        self.assertEqual(md7.query(('foo',)), {})
        self.assertEqual(md7.query(('bar',)), {})
        self.assertEqual(md8.query(()), {'value2': 'test2'})
        self.assertEqual(md8.query(('foo',)), {})
        self.assertEqual(md8.query(('bar',)), {})

    def test_all_elements(self):
        md1 = metadata.Metadata()

        md2 = md1.update((metadata.ALL_ELEMENTS, 'bar'), {'value': 'test1'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})

        md3 = md2.update(('foo', 'bar'), {'value': 'test2'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})

        md4 = md3.update((metadata.ALL_ELEMENTS, 'bar'), {'value': 'test3'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})

        md5 = md4.update(('foo', metadata.ALL_ELEMENTS), {'value': 'test4'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})

        md6 = md5.update(('foo', 'bar'), {'value': 'test5'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})

        md7 = md6.update((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS), {'value': 'test6'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})
        self.assertEqual(md7.query(('foo', 'bar')), {'value': 'test6'})

        md8 = md7.update(('foo', 'bar'), {'value': 'test7'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})
        self.assertEqual(md7.query(('foo', 'bar')), {'value': 'test6'})
        self.assertEqual(md8.query(('foo', 'bar')), {'value': 'test7'})

        self.assertEqual(md8.to_json(), [{
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'value': 'test6'
            }
        }, {
            'selector': ['foo', 'bar'],
            'metadata': {
                'value': 'test7'
            }
        }])

    def test_removal(self):
        md1 = metadata.Metadata().update((), {'value': 'test1'})

        self.assertEqual(md1.query(()), {'value': 'test1'})

        md2 = md1.update((), {'value': None})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})

        md3 = md2.update((), {'value': {'value2': 'test2'}})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})

        md4 = md3.update((), {'value': {'value2': None}})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})
        self.assertEqual(md4.query(()), {'value': {}})

        md5 = md4.update((), {'value': None})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})
        self.assertEqual(md4.query(()), {'value': {}})
        self.assertEqual(md5.query(()), {})

    def test_check(self):
        data = {
            '0': [
                [1, 2, 3],
                [4, 5, 6],
            ],
        }

        md1 = metadata.DataMetadata(for_value=data).update((), {
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': type(data),
            'value': 'test'
        })

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md1.update(('missing',), {'value': 'test'})

        md3 = md1.update(('0', 1), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md3.update(('0', 2), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md3.update(('0', 1, 3), {'value': 'test'})

        md1 = metadata.DataMetadata().update((), {
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': type(data),
            'value': 'test'
        })

        md1.check(data)

        md2 = md1.update(('missing',), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md2.check(data)

        md3 = md1.update(('0', 1), {'value': 'test'})

        md4 = md3.update(('0', 2), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md4.check(data)

        md5 = md3.update(('0', 1, 3), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md5.check(data)

        md6 = md3.update(('0', 1, 2, metadata.ALL_ELEMENTS), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'ALL_ELEMENTS set but dimension missing at'):
            md6.check(data)

    def test_errors(self):
        with self.assertRaisesRegex(TypeError, 'Metadata should be a dict'):
            metadata.Metadata().update((), None)

        class Custom:
            pass

        with self.assertRaisesRegex(TypeError, 'is not known to be immutable'):
            metadata.Metadata().update((), {'foo': Custom()})

        with self.assertRaisesRegex(TypeError, 'Selector is not a tuple or a list'):
            metadata.Metadata().update({}, {'value': 'test'})

        with self.assertRaisesRegex(TypeError, 'is not a str, int, or ALL_ELEMENTS'):
            metadata.Metadata().update((1.0,), {'value': 'test'})

        with self.assertRaisesRegex(TypeError, 'is not a str, int, or ALL_ELEMENTS'):
            metadata.Metadata().update((None,), {'value': 'test'})

    def test_data(self):
        data = {
            '0': [
                [1, 2, 3],
                [4, 5, 6],
            ],
        }

        md1 = metadata.DataMetadata(for_value=data)

        with self.assertRaisesRegex(jsonschema.exceptions.ValidationError, 'is a required property'):
            md1.update((), {'value': 'test'})

        md2 = md1.update((), {
            'id': 'test-dataset',
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': type(data),
            'dimension': {
                'length': 1
            }
        })

        md3 = md2.update(('0',), {
            'structural_type': type(data['0']),
            'dimension': {
                'length': 2
            }
        })

        self.assertEqual(md3.to_json(), [{
            'selector': [],
            'metadata': {
                'id': 'test-dataset',
                'schema': metadata.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'dict',
                'dimension': {
                    'length': 1
                },
            },
        }, {
            'selector': ['0'],
            'metadata': {
                'structural_type': 'list',
                'dimension': {
                    'length': 2,
                },
            },
        }])


if __name__ == '__main__':
    unittest.main()
